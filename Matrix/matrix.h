#pragma once
#include <vector>
#include "iostream"
using std::vector;

class Matrix
{
    int rows = 0, cols = 0;
    vector<vector<double>> matrix;
public:
    Matrix();
    Matrix(int cols, int rows);
    void set_size(int, int);
    vector<int> get_size();
    void set_value(int, int, double);
    void show_matrix();
    void set_matrix();
    double get_value(int, int);
    vector<double> get_row(int);
    vector<double> get_col(int);
    Matrix operator+ (Matrix B);
    vector<double> operator[] (int index);
    Matrix operator* (Matrix B);
    Matrix power(int);
    Matrix get_copy();
    Matrix transposition();
    Matrix operator* (double B);
};

