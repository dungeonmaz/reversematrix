#include <iostream>
#include "matrix.h"

int main()
{
	Matrix matrix1;
	matrix1.set_matrix();
	Matrix matrix2;
	matrix2.set_matrix();
	matrix1 = matrix1 * 2 * matrix2;
	matrix1.show_matrix();
	return 0;
}