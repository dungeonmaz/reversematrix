#include "matrix.h"
using namespace std;

Matrix::Matrix()
{

}

void Matrix::set_size(int x, int y)
{
    rows = y;
    cols = x;
    matrix = vector<vector<double>>(x, vector<double>(y, 0));
}

Matrix::Matrix(int cols, int rows)
{
    this->set_size(cols, rows);
}

void Matrix::set_value(int i, int j, double value)
{
    matrix[i][j] = value;
}

vector<int> Matrix::get_size()
{
    vector<int> sizes;
    sizes.push_back(rows);
    sizes.push_back(cols);
    return sizes;
}

void Matrix::show_matrix()
{
    for (int i = 0; i < cols; i++)
    {
        for (int j = 0; j < rows; j++)
        {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
}

void Matrix::set_matrix()
{
    int x, y;
    cout << "Matrix size ";
    cin >> x;
    cin >> y;
    set_size(x, y);
    for (int i = 0; i < cols; i++)
    {
        for (int j = 0; j < rows; j++)
        {
            double k;
            cout << "Element [" << i + 1 << "][" << j + 1 << "]";
            cin >> k;
            set_value(i, j, k);
        }
    }
}

double Matrix::get_value(int i, int j)
{
    double x;
    x = matrix[i][j];
    return x;
}

vector<double> Matrix::get_col(int x)
{
    vector<double> col;
    for (int i = 0; i < cols; i++)
    {
        col.push_back(matrix[i][x]);
    }
    return col;
}

Matrix Matrix::operator+(Matrix B)
{
    Matrix m3(this->cols, this->rows);
    if (this->get_size() == B.get_size())
    {
        for (int i = 0; i < cols; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                m3.set_value(i, j, this->get_value(i, j) + B.get_value(i, j));
            }
        }
    }
    else 
    {
        cout << "Matrices are not the same size";
        return Matrix();
    }
    return m3;
}

vector<double> Matrix::operator[](int index)
{
    return this->get_row(index);
}

vector<double> Matrix::get_row(int x)
{
    vector<double> row;
    for (int i = 0; i < rows; i++)
    {
        row.push_back(matrix[x][i]);
    }
    return row;
}

Matrix Matrix::operator*(Matrix B)
{
    Matrix m3(this->cols, B.rows);
    if (this->rows == B.cols)
    {
        for (int i = 0; i < this->cols; i++)
        {
            for (int j = 0; j < B.rows; j++)
            {
                double p = 0;
                for (int k = 0; k < B.cols; k++)
                {
                    p += this->get_row(i)[k] * B.get_col(j)[k];
                }
                m3.set_value(i, j, p);
            }
        }
    }
    else
    {
        cout << "Multiply does not exist";
        return Matrix();
    }
    return m3;
}

Matrix Matrix::power(int x)
{
    Matrix new_matrix = this->get_copy();
    if (this->rows == this->cols)
    {
        for (int i = 0; i < x - 1; i++)
        {
            new_matrix = new_matrix * this->get_copy();
        }
    }
    else
    {
        cout << "Power does not exist";
        return Matrix();
    }
    return new_matrix;
}

Matrix Matrix::get_copy()
{
    Matrix matrix(cols, rows);
    for (int i = 0; i < cols; i++)
    {
        for (int j = 0; j < rows; j++)
        {
            matrix.set_value(i, j, get_value(i, j));
        }
    }
    return matrix;
}

Matrix Matrix::transposition()
{
    Matrix matrix(this->rows, this->cols);
    for (int i = 0; i < this->cols; i++) 
    {
        for (int j = 0; j < this->rows; j++) 
        {
            matrix.set_value(j, i, get_value(i, j));
        }
    }
    return matrix;
}

Matrix Matrix::operator*(double B)
{
    Matrix matrix(this->rows, this->cols);
    for (int i = 0; i < this->cols; i++)
    {
        for (int j = 0; j < this->rows; j++)
        {
            matrix.set_value(i, j, get_value(i, j) * B);
        }
    }
    return matrix;
}
